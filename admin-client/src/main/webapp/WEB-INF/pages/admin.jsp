<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<script src="/resources/jquery-2.2.3.min.js"></script>
<script src="/resources/jquery.lettering.js"></script>
<script src="/resources/jquery.textillate.js"></script>

<link type="text/css" rel="stylesheet" href="<c:url value="/resources/animate.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/new_style.css" />" />


    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>
            Admin Menu
        </title>
    </head>

    <body class = "userBody">

        <div class = "centerAlignDiv">
            <div>

                <h2 id = "dvdFormTitle" class = "tlt" data-in-effect = "fadeInUp">
                    Input DVD Information
                </h2>

                <form id = "dvdForm" name = "flightForm" method = "post">
                    <div>
                        <label for = "titleInput">Title:</label>
                        <input type = "text" id = "titleInput" name = "titleInput" value = "Input title here..."/>
                    </div>
                    <div>
                        <label for = "yearInput">Year:</label>
                        <input type = "text" id = "yearInput" name = "yearInput" value = "Input year here..."/>
                    </div>
                    <div>
                        <label for = "priceInput">Price</label>
                        <input type = "text" id = "priceInput" name = "priceInput" value = "Input price here..."/>
                    </div>
                    <div>
                        <input type = "submit" value = "Submit DVD Information" />
                    </div>
                    <div>
                        <input type = "submit" name = "logOutButton" value = "Log Out" />
                    </div>
                    <div id = "statusDiv">
                        ${errorMessage}
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>