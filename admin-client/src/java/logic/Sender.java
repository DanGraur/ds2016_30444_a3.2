package logic;

import com.rabbitmq.client.*;
import entity.DVD;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.concurrent.TimeoutException;

/**
 * Created by noi on 11/18/2016.
 */
public class Sender {

    private static final String QUEUE_NAME = "DVD_QUEUE";

    // Authentication parameters
    private static final String USERNAME = "producer";
    private static final String PASSWORD = "password";

    // Private per instance, channel reference
    private Channel channel;

    public Sender() throws IOException, TimeoutException {
        // Get a new Connection Factory
        ConnectionFactory factory = new ConnectionFactory();

        // Set up the connection parameters, will run on the default port, 5672
        factory.setUsername(USERNAME);
        factory.setPassword(PASSWORD);
        factory.setHost("localhost");

        // Establish a connection using the given parameters
        Connection connection = factory.newConnection();
        channel = connection.createChannel();

        // Get the queue if it's already been created, or create one
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    }

    public String sendDVD(DVD dvd) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream out;

        String status = "Successful Delivery";

        try {
            out = new ObjectOutputStream(outputStream);
            out.writeObject(dvd);
            out.flush();

            byte[] dvdBodyToSend = outputStream.toByteArray();

            channel.basicPublish("", QUEUE_NAME, null, dvdBodyToSend);

            out.close();
        } catch (IOException e) {
            e.printStackTrace();

        } finally {

            try {
                outputStream.close();
            } catch (Exception e) {
            }

            return status;
        }
    }
}
