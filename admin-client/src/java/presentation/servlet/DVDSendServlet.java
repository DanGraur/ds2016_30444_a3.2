package presentation.servlet;

import entity.DVD;
import logic.Sender;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by noi on 11/19/2016.
 */
public class DVDSendServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/pages/admin.jsp").forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // Check if user actually wanted to log out
        if (request.getParameter("logOutButton") != null) {
            request.getSession().invalidate();

            // Relative address this time
            response.sendRedirect("login");
        }

        try {
            DVD dvd = new DVD(request.getParameter("titleInput"),
                              Integer.parseInt(request.getParameter("yearInput")),
                              Double.parseDouble(request.getParameter("priceInput")));

            Sender sender = new Sender();

            request.setAttribute("errorMessage", sender.sendDVD(dvd));
        } catch (TimeoutException e) {
            e.printStackTrace();

            request.setAttribute("errorMessage", "Could not establish connection");
        } catch (IOException e) {
            e.printStackTrace();

            request.setAttribute("errorMessage", "Could not find specified queue");
        } catch (NumberFormatException e) {
            e.printStackTrace();

            request.setAttribute("errorMessage", "Invalid input data");
        } finally {
            try {
                request.getRequestDispatcher("/WEB-INF/pages/admin.jsp").forward(request, response);
            } catch (Exception e) {
            }
        }

    }

}
