package logic;

import entity.DVD;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 * 	Provides method for sending an e-mail to a desired address.
 * 	By default, it uses the GMail SMTP. If desired, change the
 * 	the properties in the constructor.
 */
public class MailService {
    private final String username;
    private final String password;
    private final Properties props;

    private List<String> toList;
    private String addressList;

    /**
     * Builds a mail service class, used for sending e-mails.
     * The credentials provided should be the ones needed to
     * autenthicate to the SMTP server (GMail by default).
     *
     * @param username username to log in to the smtp server
     * @param password password to log in to the smtp server
     */
    public MailService(String username, String password) {
        this.username = username;
        this.password = password;

        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        toList = new ArrayList<String>();

        toList.add("dangraur1@yahoo.com");
        toList.add("dan.ovidiu.graur@gmail.com");

        parseAddressList();
    }

    private void parseAddressList() {
        addressList = "";

        Iterator<String> iter = toList.iterator();

        while(iter.hasNext()) {
            String address = iter.next();

            if (iter.hasNext())
                addressList += address + ',';
            else
                addressList += address;
        }
    }

    public void addToAddressList(String newAddress) {
        toList.add(newAddress);

        parseAddressList();
    }

    public void removeAddress(String address) {
        toList.remove(address);

        parseAddressList();
    }

    /**
     * Sends an email with the subject and content specified, to
     * the address specified.
     *
     * @param to address to send email to
     * @param subject subject of the email
     * @param content content of the email
     */
    public void sendMail(String to, String subject, String content) {
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject(subject);
            message.setText(content);

            Transport.send(message);

            System.out.println("Mail sent.");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void sendToAll(DVD dvd, String formattedDate) {
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

            try {
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(username));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(addressList));
                message.setSubject("New DVD: " + dvd.getTitle());
                message.setText("A new DVD has arrived at: " + formattedDate +
                                "\nTitle: " + dvd.getTitle() +
                                "\nYear: " + dvd.getYear() +
                                "\nPrice: " + dvd.getPrice());

                Transport.send(message);

                System.out.println("Mails sent.");
            } catch (MessagingException e) {
                e.printStackTrace();
            }
    }


}
