package logic;

import entity.DVD;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by noi on 11/18/2016.
 */
public class TextFileWriter {
    private static TextFileWriter fileWriter = new TextFileWriter();

    private TextFileWriter() {
    }

    public static  TextFileWriter getInstance() {
        return fileWriter;
    }

    public boolean writeDVDInfo(DVD dvd, String formattedDate) {
        String newFilePath = "docs\\" + dvd.getTitle() + "_" + formattedDate + ".txt";

        PrintWriter printWriter;

        try {
            printWriter = new PrintWriter(new File(newFilePath));


            // Print the contents of the DVD object to the newly generated .txt file
            printWriter.print("Title: " + dvd.getTitle() +
                              "\r\nYear: " + dvd.getYear() +
                              "\r\nPrice: " + dvd.getPrice());

            printWriter.flush();
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();

            return false;
        }

        return true;
    }
}
