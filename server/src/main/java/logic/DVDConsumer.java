package logic;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import entity.DVD;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by noi on 11/18/2016.
 */
public class DVDConsumer extends DefaultConsumer {
    private MailService mailService;

    public DVDConsumer(Channel channel) {
        super(channel);

        // ADD THE ACCOUNT AND THE PASSWORD
        mailService = new MailService("", "");
    }

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)  throws IOException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(body);
        ObjectInputStream in;
        DVD dvd = null;

        try {
            in = new ObjectInputStream(inputStream);
            dvd = (DVD) in.readObject();

            // Get the formatted date, for time-stamping reasons
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            String formatedDate = dateFormat.format(new Date());

            // Write the .txt file
            TextFileWriter.getInstance().writeDVDInfo(dvd, formatedDate);

            // Send the notification mails
            mailService.sendToAll(dvd, formatedDate);

            in.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (Exception e) {
                }

            System.out.println(dvd);
        }
    }
}
