package connection;

import com.rabbitmq.client.*;
import logic.DVDConsumer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by noi on 11/18/2016.
 */
public class Server {
    // Queue name
    private static final String QUEUE_NAME = "DVD_QUEUE";

    // Use when consumer user is set-up
    private static final String USERNAME = "consumer";
    private static final String PASSWORD = "password";

    // Use only when consumer user is not set-up
    private static final String DEFAULT_USERNAME = "guest";
    private static final String DEFAULT_PASSWORD = "";

    public Server() throws IOException, TimeoutException {
        // Create new Connection Factory
        ConnectionFactory factory = new ConnectionFactory();

        // Set up the parameters for connection; will run on default port 5672
        factory.setUsername(USERNAME);
        factory.setPassword(PASSWORD);
        factory.setHost("localhost");

        // Get new connections, based on those parameters
        Connection connection = factory.newConnection();

        // Creates a communication channel for that connection
        Channel channel = connection.createChannel();

        System.out.println("Server is initiated; Waiting to process messages");

        // If queue is not created already, create it now, otherwise just fetch a reference to it
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        // Set-up a consumer
        Consumer consumer = new DVDConsumer(channel);

        // Start the consumer
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }

}
