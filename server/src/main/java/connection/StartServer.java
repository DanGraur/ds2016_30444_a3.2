package connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by noi on 11/18/2016.
 */
public class StartServer {

    public static void main(String[] args) {
        try {
            new Server();
        } catch (IOException | TimeoutException e) {
            System.out.println("EXCEPTION ENCOUNTERED!");

            e.printStackTrace();
        }
    }
}
